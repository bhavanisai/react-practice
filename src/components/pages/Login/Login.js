import { Component } from "react";
import { Formik } from 'formik';

class Login extends Component {


    validateForm(values) {
        const errors = {};
        const pattern = /^[a-z0-9._%+-]+@[a-z0-9]+\.[a-z]{2,4}$/i;
        if (!values.email) {
            errors.email = 'Email Required';
        } else if (!pattern.test(values.email)) {
            errors.email = 'Invalid email';
        }
        if (!values.password) {
            errors.password = 'Password Required';
        } else if (values.password.length < 4) {
            errors.password = 'Password minimum length 4';
        }
        // console.log('errors : ', errors);
        return errors;
    };




    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="px-3 py-3">
                <h3 className="">Login</h3>
                <div className="">
                    <Formik
                        initialValues={{ email: '', password: '' }}
                        validate={this.validateForm}
                        onSubmit={(values, { setSubmitting }) => {
                            console.log('onsubmit values : ', values);
                            // setSubmitting(false);
                            this.props.history.push('list');
                        }}
                    >
                        {
                            ({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting,
                                isValid
                            }) => {
                                // console.log('errors : ', errors);
                                return (
                                    <form className='p-3' onSubmit={handleSubmit}>
                                        <div className=''>
                                            <input type="email" name="email" onBlur={handleBlur} onChange={handleChange} value={values.email} placeholder="Enter your email"></input>
                                            {(errors.email && touched.email) ? <div className="text-danger">{errors.email}</div> : null}
                                        </div>
                                        <div className='py-2'>
                                            <input type="password" name="password" onBlur={handleBlur} onChange={handleChange} placeholder='Enter your password' value={values.password} />
                                            {(errors.password && touched.password) ? <div className="text-danger">{errors.password}</div> : null}
                                        </div>
                                        <div>
                                            <button disabled={!isValid || isSubmitting}>Login</button>
                                        </div>
                                    </form>
                                )
                            }
                        }
                    </Formik>
                </div>
            </div>
        );
    }
}

export default Login;