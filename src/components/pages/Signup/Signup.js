import { Component } from "react";
import { Formik } from 'formik';
import { MenuItem, Select, TextField, Button, Grid, InputLabel } from "@material-ui/core";

const initialValues = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    dob: '',
    gender: ''
};

class Signup extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    validateForm(values) {
        const errors = {};
        const pattern = /^[a-z0-9._%+-]+@[a-z0-9]+\.[a-z]{2,4}$/i;
        if (!values.firstName) {
            errors.firstName = "First Name Required";
        }
        if (!values.lastName) {
            errors.lastName = "Last Name Required";
        }
        if (!values.email) {
            errors.email = "Email Required";
        } else if (!pattern.test(values.email)) {
            errors.email = "Invalid Email";
        }
        if (!values.password) {
            errors.password = "Password Required";
        } else if (values.password.length < 4) {
            errors.password = "Password minimum length 4";
        }
        if (!values.dob) {
            errors.dob = "Date of Birth Required";
        }
        if (!values.gender) {
            errors.gender = "Gender Required";
        }

        return errors;
    }


    render() {
        return (
            <div className="px-3 py-3">
                <h3>Registration Form</h3>
                <div className="">
                    <Formik
                        initialValues={{ ...initialValues }}
                        validate={this.validateForm}
                        onSubmit={(values, { setSubmitting }) => {
                            console.log('submit values : ', values);
                            alert('registration successfull....');
                        }}
                    >
                        {
                            ({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting,
                                isValid
                            }) => {
                                return (
                                    <div className="mx-3 my-4">
                                        <form onSubmit={handleSubmit}>
                                            <Grid container spacing={2}>
                                                <Grid item xs={12}>
                                                    <TextField className="w-25" type="text" variant="standard" label="First Name" name="firstName" value={values.firstName} onChange={handleChange} onBlur={handleBlur} error={errors.firstName && touched.firstName}></TextField>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <TextField type="text" className="w-25" variant="standard" label="Last Name" name="lastName" value={values.lastName} onChange={handleChange} onBlur={handleBlur} error={errors.lastName && touched.lastName}></TextField>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <TextField type="email" className="w-25" variant="standard" label="Email" name="email" value={values.email} onChange={handleChange} onBlur={handleBlur} error={errors.email && touched.email}></TextField>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <TextField type="password" className="w-25" variant="standard" label="Password" name="password" value={values.password} onChange={handleChange} onBlur={handleBlur} error={errors.password && touched.password}></TextField>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <InputLabel id="dob" error={errors.dob && touched.dob}>Date of Birth</InputLabel>
                                                    <TextField labelid="dob" type="date" className="w-25" variant="standard" name="dob" value={values.dob} onChange={handleChange} onBlur={handleBlur} error={errors.dob && touched.dob}></TextField>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <InputLabel id="gender" error={errors.gender && touched.gender}>Gender</InputLabel>
                                                    <Select labelId="gender" id="selectGender" variant="standard" className="w-25" label="Gender" name="gender" value={values.gender} onChange={handleChange} onBlur={handleBlur} error={errors.gender && touched.gender}>
                                                        <MenuItem value='male'>Male</MenuItem>
                                                        <MenuItem value='female'>Female</MenuItem>
                                                    </Select>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Button type="submit" variant="contained" color="primary" disabled={!isValid || isSubmitting}>SignUp</Button>
                                                </Grid>
                                            </Grid>
                                        </form>
                                    </div>
                                )
                            }
                        }

                    </Formik>

                </div>
            </div >
        );
    }
}

export default Signup;