import { Component } from "react";
import { NavLink } from "react-router-dom";

class Navbar extends Component {

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light border border-dark">
                    <div className="container-fluid">
                        <NavLink to="/" className="navbar-brand">Home</NavLink>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div className="navbar-nav">
                                <NavLink className="nav-link" activeClassName="active" to="/login">Login</NavLink>
                                <NavLink className="nav-link" to="/signup">Signup</NavLink>
                                <NavLink className="nav-link" to="/list">List</NavLink>
                                {/* <NavLink className="nav-link" to="/box">Box</NavLink> */}
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

export default Navbar;