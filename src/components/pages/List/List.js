import { Component } from "react";
import Box from "../Box/Box";


class List extends Component {

    colors = ['lightpink', 'lightblue', 'DodgerBlue', 'lightgreen'];

    render() {
        return (
            <div className="card p-4">
                <h3>Box List</h3>
                <div className="px-3 my-3">
                    <div className="row box my-3 justify-content-evenly ">
                        {this.colors.map((each, index) => (
                            <div className="col-2 col-md-2" key={each}>
                                <Box color={each} index={index}></Box>
                            </div>
                        ))
                        }
                    </div>
                </div>
                <div className="py-3">

                </div>
            </div>
        );
    }
};

export default List;