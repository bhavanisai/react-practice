import { Component } from "react";
import Box from "../Box/Box";
import Button from '@material-ui/core/Button';
import global from '../../../global';

class Home extends Component {

    render() {
        return (
            <div>
                <div className="my-4 mx-3">
                    <div className="col-2 col-md-2">
                        <Box color='red' index={0}></Box>
                    </div>
                    <Button variant="contained" color="primary" onClick={() => global.dispatch({ type: 'inc' })}>Click</Button>
                </div>
            </div>
        );
    }
}

export default Home;