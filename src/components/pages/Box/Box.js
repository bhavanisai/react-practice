import { Component } from 'react';
import './Box.css';
import { connect } from 'react-redux';

class Box extends Component {

    constructor(props) {
        super(props);
        this.state = {
            color: props.color,
            index: props.index
        };
    }

    changeColor(each) {
        this.setState({
            color: 'gray'
        });
        setTimeout(() => {
            this.setState({
                color: each
            });
        }, 3000);
    }


    render() {
        return (
            <>
                <div className="card py-3 border border-dark text-center text-dark" style={{ backgroundColor: this.state.color, height: '120px' }}
                    onClick={() => this.changeColor(this.state.color)}>
                    Box {this.state.index + 1}
                </div>
                <div className="py-3">
                    Inside Box component count : {this.props.count}
                </div>
            </>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    // console.log('state : ', state);
    return {
        count: state
    }
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Box);