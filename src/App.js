import 'bootstrap/dist/css/bootstrap.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './App.css';
import Box from "./components/pages/Box/Box";
import Home from "./components/pages/Home/Home";
import List from "./components/pages/List/List";
import Login from "./components/pages/Login/Login";
import Navbar from './components/pages/Navbar/Navbar';
import Signup from "./components/pages/Signup/Signup";
import global from './global';
import { Provider } from 'react-redux';

function App() {
  return (
    <Provider store={global}>
      <div className="App">
        <Router>
          <Navbar></Navbar>

          <Switch>
            <Route path="/" component={Home} exact={true}></Route>
            <Route path="/login" component={Login}></Route>
            <Route path="/signup" component={Signup}></Route>
            <Route path="/list" component={List}></Route>
            <Route path="/box" component={Box}></Route>
          </Switch>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
