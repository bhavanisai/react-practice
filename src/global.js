import { createStore } from 'redux';


const global = createStore(myReducer);

function myReducer(state = 0, action) {
    if (action.type === 'inc') {
        return state + 1;
    } else {
        return state;
    }
}

export default global;
